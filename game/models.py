from django.db import models
from django.utils import timezone
import datetime

# Create your models here.
class User(models.Model):
	agency = models.CharField(max_length=10)
	phone_number = models.CharField(max_length=12, unique=True,  blank=False, null=False)
	birth_year = models.CharField(max_length=6, blank=True)
	gender = models.CharField(max_length=4, blank=True)
	created_date = models.DateTimeField(default=timezone.now)
	coupon_count = models.IntegerField(default=0)
	
	def __str__(self):
		return self.phone_number	

class Category(models.Model):
	c_name = models.CharField(max_length=30)

	def __str__(self):
		return self.c_name

class Store(models.Model):
	category = models.ForeignKey(Category, on_delete=models.CASCADE)
	s_name = models.CharField(max_length=30)

	def __str__(self):
		return self.s_name


class Sale(models.Model):
	store = models.ForeignKey(Store, on_delete=models.CASCADE)
	text = models.TextField(null=False)
	exp_date = models.DateTimeField(null=False) 
	cnt= models.IntegerField(default=0)

	def __str__(self):
		return self.store.s_name

class Coupon(models.Model):
	sale = models.ForeignKey(Sale, on_delete=models.CASCADE,null=True)
	user = models.ForeignKey(User, on_delete=models.CASCADE,null=True)
	pub_date = models.DateTimeField(default=timezone.now)
	use_date = models.DateTimeField(null=True)

	def __str__(self):
		return self.sale.store.s_name

	