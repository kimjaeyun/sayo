from django.contrib import admin
from .models import User,Coupon,Store,Category,Sale


admin.site.register(User)
admin.site.register(Coupon)


admin.site.register(Category)

class SaleAdmin(admin.ModelAdmin):
	list_per_page = 20
	list_display = (
	'id','store', 'text','exp_date','cnt',)
	search_fields = ('store__s_name',)
	ordering = ('-id', 'store__s_name',)

class StoreAdmin(admin.ModelAdmin):
	list_per_page = 20
	list_display = (
	'id','s_name', 'category')
	search_fields = ('s_name',)
	ordering = ('-id', 's_name',)

admin.site.register(Store, StoreAdmin)	
admin.site.register(Sale, SaleAdmin)