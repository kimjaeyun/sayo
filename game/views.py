from django.shortcuts import redirect, render
from .models import User,Coupon,Store,Sale,Category
from django.core.exceptions import ObjectDoesNotExist
from .forms import UserForm
from django.utils import timezone
import datetime
from random import randint
# Create your views here.

def login(request):
    return render(request, 'game/join2.html',{
      })

def menu(request):
    return render(request, 'game/menu.html',{
      })

def menu_choose(request):
  if request.method == "POST":
     category_name = request.POST['category']
     category = Category.objects.get(c_name=category_name)
     store_set = Store.objects.filter(category=category).order_by('s_name')
     count = store_set.count()
     random = randint(0,count-1)
     store = store_set[random]
     return render(request, 'game/menu_choose.html',{
      'random_store': store,
      'store_set':store_set,
      'category': category_name,
      })
  else:
    return render(request, 'game/error.html')



def join(request):
    if request.method == "POST":
        birth_year = request.POST['birth_year']
        birth_year = int(birth_year)
        form = UserForm(request.POST)
        if form.is_valid():
          person = form.save(commit=False)
          phone_number = person.phone_number
          request.session['phone_number'] = person.phone_number
          person.save()
          sale_set = Sale.objects.filter(cnt__gt=1,exp_date__gt=timezone.now())
          return render(request, 'game/home.html', {
            'array': sale_set,
            })
        else:
          return render(request, 'game/error3.html')
    else:
      return render(request, 'game/error.html')


def home(request):
    if request.method == "GET":
        sale_set = Sale.objects.filter(cnt__gt=0,exp_date__gt=timezone.now())
        return render(request, 'game/home.html', {
              'array': sale_set,
        })
    else:
      return render(request, 'game/error.html')

def order_category(request): #카테고리 순으로 정렬
  sale_set = Sale.objects.filter(cnt__gt=0,exp_date__gt=timezone.now()).order_by('store__category__c_name')
  return render(request, 'game/home.html', {
              'array': sale_set,
              'd' : 1
  })

def re_order_category(request):
  sale_set = Sale.objects.filter(cnt__gt=0,exp_date__gt=timezone.now()).order_by('-store__category__c_name')
  return render(request, 'game/home.html', {
              'array': sale_set,
  })

def order_cnt(request): #수량 순으로 정렬
  sale_set = Sale.objects.filter(cnt__gt=0,exp_date__gt=timezone.now()).order_by('cnt')
  return render(request, 'game/home.html', {
              'array': sale_set,
              'a' : 1
  })



def re_order_cnt(request):
  sale_set = Sale.objects.filter(cnt__gt=0,exp_date__gt=timezone.now()).order_by('-cnt')
  return render(request, 'game/home.html', {
              'array': sale_set,
  })

def order_store(request): #가나다 순으로 정렬
  sale_set = Sale.objects.filter(cnt__gt=0,exp_date__gt=timezone.now()).order_by('store__s_name')
  return render(request, 'game/home.html', {
              'array': sale_set,
              'b' :1
  })

def re_order_store(request): #가나다 순으로 정렬
  sale_set = Sale.objects.filter(cnt__gt=0,exp_date__gt=timezone.now()).order_by('-store__s_name')
  return render(request, 'game/home.html', {
              'array': sale_set,
  })

def order_date(request): #마감 순으로 정렬
  sale_set = Sale.objects.filter(cnt__gt=0,exp_date__gt=timezone.now()).order_by('exp_date')
  return render(request, 'game/home.html', {
              'array': sale_set,
              'c': 1
  })

def re_order_date(request): #마감 순으로 정렬
  sale_set = Sale.objects.filter(cnt__gt=0,exp_date__gt=timezone.now()).order_by('-exp_date')
  return render(request, 'game/home.html', {
              'array': sale_set,
  })

def idcheck(request):
  if request.method == "POST":
        try:
            phone_number = request.POST.get('phone_number', None)
            person = User.objects.get(phone_number= phone_number) 
            if not(len(phone_number) == 11 or len(phone_number) == 12):
                return render(request, 'game/error3.html')
            request.session['phone_number'] = person.phone_number
            sale_set = Sale.objects.filter(cnt__gt=1,exp_date__gt=timezone.now())
            return render(request, 'game/home.html',{
              'array' :  sale_set,
              })
        except ObjectDoesNotExist:
            phone_number = request.POST.get('phone_number', None)
            agency = request.POST.get('agency', None)
            if not(len(phone_number) == 11 or len(phone_number) == 12) :
                return render(request, 'game/error3.html')
            return render(request, 'game/join.html',{
              'phone_number': phone_number,
              'agency': agency,
              })
  else:
      return render(request, 'game/error.html')


def coupon(request):
  if request.session.get('phone_number',''):
    phone_number = request.session.get('phone_number','')
    person = User.objects.get(phone_number= phone_number)
    coupon_set = Coupon.objects.filter(user= person, use_date=None)
    return render(request, 'game/coupon.html',{
      'array' : coupon_set,
      })
  else:
    return render(request, 'game/error3.html')

def coupon_detail(request, id):
  if request.method == "GET":
    try:
      coupon = Coupon.objects.get(id=id)
      sale = Sale.objects.get(coupon=coupon)
      phone_number = request.session.get('phone_number','')
      user = User.objects.get(phone_number= phone_number)
      if (timezone.now()> sale.exp_date):
        return render(request, 'game/exe_coupon.html')
      return render(request, 'game/coupon_detail.html',{
        'sale': sale,
        'coupon':coupon,
        })
    except ObjectDoesNotExist:
      return render(request, 'game/error.html')
  else:
    return render(request, 'game/error.html')

def sale_detail(request, id):
  if request.method == "GET":
    try:
      sale = Sale.objects.get(id=id)
      if sale.cnt > 0:
        return render(request, 'game/sale_detail.html',{
          'sale' : sale,
          })
      phone_number = request.session.get('phone_number','')
      user = User.objects.get(phone_number= phone_number)
      if (timezone.now()> sale.exp_date):
        return render(request, 'game/exe_coupon.html')
    except ObjectDoesNotExist:
      return render(request, 'game/error.html')
  else:
    return render(request, 'game/error.html')


def sale_get(request):
  if request.method == "POST":
    sale_id = request.POST.get('sale', None)
    sale_id = int(sale_id)
    sale = Sale.objects.get(id=sale_id)
    sale.cnt = sale.cnt - 1;
    sale.save()
    phone_number = request.session.get('phone_number','')
    user = User.objects.get(phone_number= phone_number)
    coupon = Coupon(sale=sale, user=user)
    coupon.save()
    sale_set = Sale.objects.filter(cnt__gt=0,exp_date__gt=timezone.now())
    return render(request, 'game/home.html', {
              'array': sale_set,
              'i': 2,
        })
  else:
    return render(request, 'game/error.html')






def coupon_use(request):
  if request.method == "POST":
    coupon_id = request.POST.get('coupon', None)
    coupon_id = int(coupon_id)
    coupon = Coupon.objects.get(id=coupon_id)
    if coupon.use_date is not None:
       return render(request, 'game/error_coupon.html') 
    coupon.use_date = timezone.now();
    coupon.save()
    phone_number = request.session.get('phone_number','')
    user = User.objects.get(phone_number= phone_number)
    user_coupon_set = Coupon.objects.filter(user=user, use_date=None)
    user.coupon_count = user_coupon_set.count()
    user.save()
    return render(request, 'game/coupon.html',{
      'array': user_coupon_set,
      'i': 1,
      })
  else:
    return render(request, 'game/error.html')
