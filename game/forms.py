from django import forms
from .models import User

class UserForm(forms.ModelForm):
	class Meta:
		model = User
		fields = ['agency', 'phone_number', 'birth_year', 'gender']