from django.urls import path
from . import views

urlpatterns = [
 	  path('', views.login, name='login'),
  	path('join/', views.join, name='join'),
  	path('idcheck', views.idcheck, name='idcheck'),
  	path('coupon', views.coupon, name='coupon'),
    path('coupon/detail/<int:id>', views.coupon_detail, name='coupon_detail'),
  	path('sale/detail/<int:id>', views.sale_detail, name='sale_detail'),
    path('sale/get', views.sale_get, name='sale_get'),
    path('home/', views.home, name='home'),
    path('home/order_category', views.order_category, name='order_category'),
    path('home/order_cnt', views.order_cnt, name='order_cnt'),
    path('home/order_store', views.order_store, name='order_store'),
    path('home/order_date', views.order_date, name='order_date'),
    path('home/re_order_category', views.re_order_category, name='re_order_category'),
    path('home/re_order_cnt', views.re_order_cnt, name='re_order_cnt'),
    path('home/re_order_store', views.re_order_store, name='re_order_store'),
    path('home/re_order_date', views.re_order_date, name='re_order_date'),
    path('coupon/coupon_use', views.coupon_use, name='coupon_use'),
    path('menu', views.menu, name='menu'),
    path('menu/choose', views.menu_choose, name='menu_choose'),
]